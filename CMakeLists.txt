cmake_minimum_required(VERSION 3.15)
project(aoc-2021)

include_directories(common)

add_executable(p01 01/main.c)
add_executable(p02 02/main.c)
add_executable(p03 03/main.c)

