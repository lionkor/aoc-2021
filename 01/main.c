#define FILEOPS_IMPL
#include <fileops.h>

#include <assert.h>
#include <string.h>

void part1(void) {
    size_t buffer_size = 0;
    char* buffer = (char*)read_file("01/input.txt", &buffer_size);
    if (!buffer) {
        return;
    }
    char* iter = buffer;
    char* end = buffer + buffer_size;
    uint64_t previous = strtol(iter, &end, 10);
    size_t larger_count = 0;
    while (iter < end) {
        iter = end;
        uint64_t value = strtol(iter, &end, 10);
        if (value > previous) {
            ++larger_count;
        }
        previous = value;
    }
    printf("part 1: %lu\n", larger_count);
    free(buffer);
}

void part2(void) {
    size_t buffer_size = 0;
    char* buffer = (char*)read_file("01/input.txt", &buffer_size);
    if (!buffer) {
        return;
    }

    size_t newlines = 0;
    for (size_t i = 0; i < buffer_size; ++i) {
        if (buffer[i] == '\n') {
            ++newlines;
        }
    }
    assert(newlines != 0);
    size_t values_size = newlines;
    size_t* values = malloc(sizeof(size_t) * values_size);
    memset(values, 0, values_size * sizeof(size_t));
    char* iter = buffer;
    char* end = NULL;
    size_t i = 0;
    for (;;) {
        uint64_t value = strtol(iter, &end, 10);
        if (value == 0) {
            break;
        }
        iter = end;
        values[++i] = value;
    }

    size_t larger_count = 0;
    for (i = 0; i + 3 < values_size; ++i) {
        if (values[i + 3] > values[i]) {
            ++larger_count;
        }
    }
    printf("part 2: %lu\n", larger_count);
    free(values);
    free(buffer);
}

int main(void) {
    part1();
    part2();
}
