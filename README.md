My solutions to [AOC 2021](https://adventofcode.com/2021)

To run, first execute 
```
cmake .
```
and then
```
cmake . --build
```

Then pick which problem's solution you want to run, numbered like `p{01-25}`, and run it with `./p{01-25}`, for example:

```
./p01
```

to run problem 1.
