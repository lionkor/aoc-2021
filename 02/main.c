#define FILEOPS_IMPL
#include <fileops.h>
#define STRINGOPS_IMPL
#include <stringops.h>

#include <assert.h>
#include <stdio.h>
#include <string.h>

void part1(void) {
    size_t buffer_size = 0;
    char* buffer = (char*)read_file("02/input.txt", &buffer_size);

    size_t horiz_pos = 0, depth = 0;

    char* startp = buffer;
    char* endp = startp;
    char* buffer_end = buffer + buffer_size;
    while (startp < buffer_end) {
        endp = consume_until_ws(startp);
        size_t len = endp - startp;
        startp = consume_ws(endp);
        int64_t value = strtol(startp, &endp, 10);
        startp = consume_ws(endp); // consume \n
        switch (len) {
        case 2: // up
            depth -= value;
            break;
        case 4: // down
            depth += value;
            break;
        case 7: // forward
            horiz_pos += value;
            break;
        default:
            printf("not reachable: %lu\n", len);
            break;
        }
    }
    printf("part 1: %lu\n", horiz_pos * depth);
    free(buffer);
}

void part2(void) {
    size_t buffer_size = 0;
    char* buffer = (char*)read_file("02/input.txt", &buffer_size);

    size_t horiz_pos = 0;
    size_t depth = 0;
    size_t aim = 0;

    char* startp = buffer;
    char* endp = startp;
    char* buffer_end = buffer + buffer_size;
    while (startp < buffer_end) {
        endp = consume_until_ws(startp);
        size_t len = endp - startp;
        startp = consume_ws(endp);
        int64_t value = strtol(startp, &endp, 10);
        startp = consume_ws(endp); // consume \n
        switch (len) {
        case 2: // up
            aim -= value;
            break;
        case 4: // down
            aim += value;
            break;
        case 7: // forward
            horiz_pos += value;
            depth += aim * value;
            break;
        default:
            printf("not reachable: %lu\n", len);
            break;
        }
    }
    printf("part 2: %lu\n", horiz_pos * depth);
    free(buffer);
}

int main(void) {
    part1();
    part2();
}
