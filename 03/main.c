#define FILEOPS_IMPL
#include <fileops.h>

#define BITS 12

int main(void) {
    size_t buffer_size = 0;
    char* buffer = (char*)read_file("03/input.txt", &buffer_size);

    char* iterp = buffer;
    char* endp = iterp;
    size_t value = 0;
    size_t count[BITS];
    size_t total_values = 0;
    memset(count, 0, sizeof(count));
    while (iterp < buffer + buffer_size - 1) {
        value = strtoul(iterp, &endp, 2);
        iterp = endp;
        for (size_t i = 0; i < BITS; ++i) {
            count[BITS - i - 1] += (value & (1 << i)) >> i;
        }
        ++total_values;
    }
    uint32_t gamma = 0;
    uint32_t epsilon = 0;
    // which is the most common?
    for (size_t i = 0; i < BITS; ++i) {
        if (count[i] > total_values / 2) {
            gamma |= (1 << (BITS - 1 - i));
        } else {
            epsilon |= (1 << (BITS - 1 - i));
        }
    }
    printf("part 1: %u\n", gamma * epsilon);
    free(buffer);
}
