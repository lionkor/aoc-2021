#pragma once

char* consume_until_ws(char* buffer);
char* consume_ws(char* buffer);

#ifdef STRINGOPS_IMPL

#include <ctype.h>
#include <string.h>

char* consume_until_ws(char* buffer) {
    while (!isspace(*buffer))
        ++buffer;
    return buffer;
}

char* consume_ws(char* buffer) {
    while (isspace(*buffer))
        ++buffer;
    return buffer;
}

#endif
