#pragma once

#include <stddef.h>
#include <stdint.h>

// allocates a buffer, reads the file into it.
// buffer has to be free'd at the end.
uint8_t* read_file(const char* filename, size_t* out_size);

#ifdef FILEOPS_IMPL
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

uint8_t* read_file(const char* filename, size_t* out_size) {
    struct stat file_stat;
    *out_size = 0;
    if (stat(filename, &file_stat) != 0) {
        perror("stat");
        return NULL;
    }
    size_t file_size = file_stat.st_size;
    *out_size = file_size;
    uint8_t* buffer = (uint8_t*)malloc(file_size);
    if (!buffer) {
        perror("malloc");
        return NULL;
    }
    FILE* file = fopen(filename, "r");
    if (!file) {
        perror("fopen");
        free(buffer);
        return NULL;
    }
    size_t bytes_read = fread(buffer, sizeof(uint8_t), file_size, file);
    fclose(file);
    if (bytes_read != file_size) {
        perror("fread");
        free(buffer);
        return NULL;
    }
    return buffer;
}

#endif // COMMON_IMPL
